# This project has been moved into gitlab.com/dspom/OpenVAF

# Verilog ARC

A codegen libary meant to be used with the OpenVAF crate. It uses the procmacro2 crate to generate code that can then be  compiled using rustc.
This libary is primarly being developed for use in [VerilogAE](https://dspom.gitlab.io/verilogae/). However it is specifically designed in such a way that that it can easily be applied to other projects